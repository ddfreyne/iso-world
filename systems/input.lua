local Engine = require('engine')
local Debugger = require('debugger')

local Gamestate = require('engine.vendor.hump.gamestate')

local Components = require('components')

local Input = {}
Input.__index = Input

function Input.new(entities)
  return setmetatable({ entities = entities }, Input)
end

local function screenToWorld(screenPoint, viewportSize, viewportPosition, cameraPosition, scale)
  local viewportPoint = Engine.Types.Point.new(
    screenPoint.x - viewportPosition.x + viewportSize.width  / 2,
    screenPoint.y - viewportPosition.y + viewportSize.height / 2
  )

  local unscaledWorldPoint = Engine.Types.Point.new(
    viewportPoint.x - viewportSize.width  / 2,
    viewportPoint.y - viewportSize.height / 2
  )

  local scaledWorldPoint = Engine.Types.Point.new(
    unscaledWorldPoint.x / scale.value + cameraPosition.x,
    unscaledWorldPoint.y / scale.value + cameraPosition.y
  )

  return scaledWorldPoint
end

local function move(viewport, dx, dy)
  local viewportComponent = viewport:get(Engine.Components.Viewport)
  if not viewportComponent then return end

  local camera = viewportComponent.camera

  local cameraPosition = camera:get(Engine.Components.Position)
  if not cameraPosition then return end

  cameraPosition.x = cameraPosition.x + dx
  cameraPosition.y = cameraPosition.y + dy
end

local function zoom(viewport, cursorX, cursorY, val)
  local viewportComponent = viewport:get(Engine.Components.Viewport)
  if not viewportComponent then return end

  local camera   = viewportComponent.camera
  local entities = viewportComponent.entities

  local cameraScale      = camera:get(Engine.Components.Scale)
  local cameraPosition   = camera:get(Engine.Components.Position)
  local viewportSize     = viewport:get(Engine.Components.Size)
  local viewportPosition = viewport:get(Engine.Components.Position)

  if not cameraScale      then return end
  if not cameraPosition   then return end
  if not viewportSize     then return end
  if not viewportPosition then return end

  local cursorScreenPos = Engine.Types.Point.new(cursorX, cursorY)
  local cursorWorldPos = screenToWorld(
    cursorScreenPos, viewportSize, viewportPosition, cameraPosition, cameraScale)

  local f = (1 - 1 / val)

  cameraPosition.x = cameraPosition.x + f * (cursorWorldPos.x - cameraPosition.x)
  cameraPosition.y = cameraPosition.y + f * (cursorWorldPos.y - cameraPosition.y)

  cameraScale.value = cameraScale.value * val
end

local function zoomIn(viewport, x, y)
  zoom(viewport, x, y, 1 * 1.1)
end

local function zoomOut(viewport, x, y)
  zoom(viewport, x, y, 1 / 1.1)
end

local function viewportUnderCursor(entities, x, y)
  local point = Engine.Types.Point.new(x, y)

  local findFn = function(entity)
    if entity:get(Engine.Components.Viewport) then
      local rect = Engine.rectForEntity(entity)
      return rect:containsPoint(point)
    else
      return false
    end
  end

  return entities:find(findFn)
end

function Input:mousepressed(x, y, button)
  local viewport = viewportUnderCursor(self.entities, x, y)
  if not viewport then return end

  if button == "wu" then zoomIn(viewport, x, y) end
  if button == "wd" then zoomOut(viewport, x, y) end
end

function Input:keypressed(key, isrepeat)
  -- Open debugger
  if key == "tab" then
    Gamestate.push(Debugger.Gamestate.new(self.entities))
    return
  end
end

function Input:update(dt)
  local viewport = viewportUnderCursor(self.entities, 1, 1)
  if not viewport then return end

  local speed = 600*dt

  local lk = love.keyboard
  local lm = love.mouse
  local lw = love.window

  if lk.isDown("left") or lm.getX() <= 0 then
    move(viewport, -speed, 0)
  end

  if lk.isDown("right") or lm.getX() >= lw.getWidth() - 1 then
    move(viewport, speed, 0)
  end

  if lk.isDown("up") or lm.getY() <= 0 then
    move(viewport, 0, -speed)
  end

  if lk.isDown("down") or lm.getY() >= lw.getHeight() - 1 then
    move(viewport, 0, speed)
  end
end

return Input
