local Engine   = require('engine')

local ArenaSpace = require('spaces.arena')
local Components = require('components')

local Main = {}

local lw = love.window

local h = 48
local w = 96

local cornerToImagePath = {
  gggg = 'assets/iso-road/grass.png',

  wggg = 'assets/iso-road/waterES.png',
  gwgg = 'assets/iso-road/waterNE.png',
  ggwg = 'assets/iso-road/waterNW.png',
  gggw = 'assets/iso-road/waterSW.png',

  wwgg = 'assets/iso-road/waterE.png',
  -- wgwg = 'assets/iso-road/water???.png',
  wggw = 'assets/iso-road/waterS.png',
  gwwg = 'assets/iso-road/waterN.png',
  -- gwgw = 'assets/iso-road/water???.png',
  ggww = 'assets/iso-road/waterW.png',

  wwwg = 'assets/iso-road/waterCornerNE.png',
  wwgw = 'assets/iso-road/waterCornerES.png',
  wgww = 'assets/iso-road/waterCornerSW.png',
  gwww = 'assets/iso-road/waterCornerNW.png',

  wwww = 'assets/iso-road/water.png',
}

local cornerToImagePath = {
  gggg = 'assets/iso-road/grass.png',

  wggg = 'assets/iso-road/beachNW.png',
  gwgg = 'assets/iso-road/beachSW.png',
  ggwg = 'assets/iso-road/beachES.png',
  gggw = 'assets/iso-road/beachNE.png',

  wwgg = 'assets/iso-road/beachE.png',
  -- wgwg = 'assets/iso-road/beach???.png',
  wggw = 'assets/iso-road/beachS.png',
  gwwg = 'assets/iso-road/beachN.png',
  -- gwgw = 'assets/iso-road/beach???.png',
  ggww = 'assets/iso-road/beachW.png',

  wwwg = 'assets/iso-road/beachCornerNE.png',
  wwgw = 'assets/iso-road/beachCornerES.png',
  wgww = 'assets/iso-road/beachCornerSW.png',
  gwww = 'assets/iso-road/beachCornerNW.png',

  wwww = 'assets/iso-road/water.png',
}

local anchorPoints = {
  gggw = Engine.Types.Point.new(0.5, 0.5 - 0.06),

  wggw = Engine.Types.Point.new(0.5, 0.5 - 0.06),
  wgww = Engine.Types.Point.new(0.5, 0.5 - 0.06),
  ggww = Engine.Types.Point.new(0.5, 0.5 - 0.06),

  wwgw = Engine.Types.Point.new(0.5, 0.5 - 0.06),
  gwww = Engine.Types.Point.new(0.5, 0.5 - 0.06),

  wwww = Engine.Types.Point.new(0.5, 0.5 - 0.06),
}

local map = {
  { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', },
  { 'g', 'g', 'g', 'g', 'w', 'w', 'w', 'w', 'w', 'w', },
  { 'g', 'g', 'w', 'w', 'w', 'w', 'w', 'w', 'w', 'g', },
  { 'g', 'g', 'w', 'w', 'w', 'w', 'w', 'g', 'w', 'w', },
  { 'w', 'g', 'w', 'w', 'g', 'w', 'g', 'g', 'g', 'g', },
  { 'w', 'g', 'w', 'w', 'w', 'w', 'g', 'g', 'g', 'g', },
  { 'w', 'w', 'w', 'g', 'g', 'g', 'g', 'g', 'g', 'g', },
  { 'w', 'w', 'w', 'g', 'g', 'g', 'g', 'w', 'g', 'g', },
  { 'w', 'w', 'w', 'g', 'g', 'g', 'g', 'g', 'g', 'g', },
  { 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', 'g', },
}

local function keyFor(x, y)
  local x1 = x <= 1 and 1 or x
  local x2 = x >= 10 and 10 or x + 1

  local y1 = y <= 1 and 1 or y
  local y2 = y >= 10 and 10 or y + 1

  local key =
    map[x1][y1] ..
    map[x2][y1] ..
    map[x2][y2] ..
    map[x1][y2]

  return key
end

local function resolve(x, y)
  local key = keyFor(x, y)
  return cornerToImagePath[key] or 'assets/iso-road/dirt.png'
end

local function anchorPointFor(x, y)
  local key = keyFor(x, y)
  return anchorPoints[key] or Engine.Types.Point.new(0.5, 0.5)
end

local function addTile(entities, x, y)
  local imagePath = resolve(x+1, y+1)

  local x2 = 0.5 * x * w + 0.5 * y * w
  local y2 = 0.5 * x * h - 0.5 * y * h

  local anchorPoint = anchorPointFor(x+1, y+1)

  local tile = Engine.Entity.new()
  tile:add(Engine.Components.Description,    'Tile')
  tile:add(Engine.Components.Position,       x2, y2)
  tile:add(Engine.Components.Z,              x - y)
  tile:add(Engine.Components.Image,          imagePath)
  tile:add(Engine.Components.AnchorPoint,    anchorPoint.x, anchorPoint.y)
  entities:add(tile)
end

local function createMap(entities)
  for x = 0, 10 do
    for y = 0, 10 do
      addTile(entities, x-1, y-1)
    end
  end
end

local function createEntities()
  local hiddenEntities = Engine.Types.EntitiesCollection.new()
  createMap(hiddenEntities)

  local camera = Engine.Entity.new()
  camera:add(Engine.Components.Camera)
  camera:add(Engine.Components.Position, 380, 0)
  camera:add(Engine.Components.Z, 100)
  camera:add(Engine.Components.Scale, 1)
  camera:add(Components.CameraInput)
  hiddenEntities:add(camera)

  --

  local entities = Engine.Types.EntitiesCollection.new()

  local size = Engine.Types.Size.new(1024, 768)

  local viewport = Engine.Entity.new()
  viewport:add(Engine.Components.Position, size.width/2, size.height/2)
  viewport:add(Engine.Components.Size,     size.width, size.height)
  viewport:add(Engine.Components.Viewport, camera, hiddenEntities)
  viewport:add(Engine.Components.Z, 0)
  entities:add(viewport)

  return entities
end

function Main.new()
  local entities = createEntities()
  local arenaSpace = ArenaSpace.new(entities)

  return Engine.Gamestate.new({ arenaSpace })
end

return Main
