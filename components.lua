require('engine')

local Components = {}

Components.CameraInput = {
  order  = 1,
  name   = 'Camera input',
  new    = function() return {} end,
  format = function(self) return 'Yes' end,
}

return Components
