function love.conf(t)
  t.title = "Isometric Map"
  t.window.width = 1024
  t.window.height = 768
end

package.path = package.path .. ';../d-tech/?.lua;../d-tech/?/init.lua'
